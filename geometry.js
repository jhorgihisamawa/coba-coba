/* Cube Volume Function */
function CalculateCubeVolume(s) {
    let CubeVolume = s ** 3;

    return CubeVolume;
}

let CubeA = CalculateCubeVolume(3);
let CubeB = CalculateCubeVolume(5);

console.log("Cube A:" + CubeA + " " + "cm3")
console.log("Cube A:" + CubeB + " " + "cm3")

/* Beam Volume Function */
function CalculateBeamVolume(t,l,h) {
    let BeamVolume = t * h * l;

    return BeamVolume;
}

let BeamA = CalculateBeamVolume(3,6,8);
let BeamB = CalculateBeamVolume(5,7,9);

console.log("Beam A:" + BeamA + "cm3")
console.log("Beam B:" + BeamB + "cm3")

/* Menghitung Volume Tabung*/

function MenghitungVolumeTabung(r,t) {
    let jarijaritabung = Math.PI * r ** 2;
    let Volumetabung = jarijaritabung * t;

    return Volumetabung;
}

let TabungA = MenghitungVolumeTabung(3,5);
let TabungB = MenghitungVolumeTabung(2,5);

console.log("Tabung A :" + TabungA + "cm3")
console.log("Tabung B :" + TabungB + "cm3")

